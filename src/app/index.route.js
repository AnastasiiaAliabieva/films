export function routerConfig($stateProvider, $urlRouterProvider) {
  'ngInject';
  $stateProvider
    .state('home', {
        url: '/',
        templateUrl: 'app/topFilms/top.films.html',
        controller: 'TopFilmsController',
        controllerAs: 'topFilms'
      },
      {
        url: '/chart',
        templateUrl: 'app/chart/chart.html',
        controller: 'ChartController',
        controllerAs: 'chart'
      },
      {
        url: '/favorite',
        templateUrl: 'app/favorite/favorite.html',
        controller: 'FavoriteController',
        controllerAs: 'favorite'
      }
    );
  // $stateProvider
  //   .state('home', {
  //     url: '/',
  //     templateUrl: 'app/main/main.html',
  //     controller: 'MainController',
  //     controllerAs: 'main'
  //   });

  $urlRouterProvider.otherwise('/');
}
