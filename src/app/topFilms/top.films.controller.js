export class TopFilmsController {
  constructor ($scope, $http) {
    'ngInject';
    $scope.hello = 'you are amazing';
    $scope.linkToAuthor = 'http://www.imdb.com/name/';


    $http.get('http://www.myapifilms.com/imdb/top?start=1&end=20&token=ebfb7525-3b98-4d85-b1ed-e1796c3b0087&data=1', {
    }).success(function (response) {
      console.log(response)
      $scope.films = response.data.movies;
    });

  }
}
